import 'package:flutter/material.dart';

class AppColor {
  static const Color lightBlack = Color(0xff393939);
  static const Color black = Color(0xff2E2E2E);
  static const Color purple = Color(0xffA85FEC);
  static const Color red = Color(0xffEC5F6B);
  static const Color ping = Color(0xffEC5FB6);
  static const Color blue = Color(0xff5F6DEC);
  static const Color green = Color(0xff3CD288);
  static const Color orange = Color(0xffEE9E44);
  static const Color gray = Color(0xff797979);
  static const Color lightBlue = Color(0xff9CBBFF);
  static const Color white = Color(0xffF5F5F5);
  static const Color darkWhite = Color(0xffF0F0F0);
  static const Color lightPink = Color(0xffFF98A1);

  static List<Color> values = [
    purple,
    red,
    ping,
    blue,
    green,
    orange,
    lightBlue,
    lightPink
  ];

  static const Map<int, Color> blackMap = {
    50: Color.fromRGBO(46, 46, 46, .1),
    100: Color.fromRGBO(46, 46, 46, .2),
    200: Color.fromRGBO(46, 46, 46, .3),
    300: Color.fromRGBO(46, 46, 46, .4),
    400: Color.fromRGBO(46, 46, 46, .5),
    500: Color.fromRGBO(46, 46, 46, .6),
    600: Color.fromRGBO(46, 46, 46, .7),
    700: Color.fromRGBO(46, 46, 46, .8),
    800: Color.fromRGBO(46, 46, 46, .9),
    900: Color.fromRGBO(46, 46, 46, 1),
  };

  static MaterialColor blackScheme = MaterialColor(black.value, blackMap);
}
