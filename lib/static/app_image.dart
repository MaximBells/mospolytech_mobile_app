class AppImage {
  static const String logo = 'assets/logo.png';
  static const String logoMin = 'assets/logo_mini.png';
  static const String allGroups = 'assets/all_group.png';
  static const String profile = 'assets/profile.png';
  static const String shedule = 'assets/shedule.png';
  static const String home = 'assets/home.png';
  static const String qrCode = 'assets/qr_code.png';
  static const String chat = 'assets/chat.png';
  static const String faq = 'assets/faq.png';
  static const String pd = 'assets/pd.png';
  static const String ruble = 'assets/ruble.png';
  static const String settings = 'assets/settings.png';
  static const String success = 'assets/success.png';
  static const String workers = 'assets/workers.png';
  static const String medicine = 'assets/medicine.png';
}
