import 'package:get/get.dart';
import 'package:get_it/get_it.dart';
import 'package:polytech_app/components/services/crypt_service.dart';
import 'package:polytech_app/components/services/request_service.dart';
import 'package:polytech_app/components/services/storage_service.dart';

final getIt = GetIt.instance;


Future<void> initServices() async {
  Get.put(RequestService());

  Get.put(CryptService());
  getIt.registerSingleton<StorageService>(StorageService());
}
