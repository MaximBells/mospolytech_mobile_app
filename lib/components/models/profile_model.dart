class ProfileModel {
  final String surname;
  final String name;
  final String lastName;
  final String status;
  final String gender;
  final String dateBirth;
  final String code;
  final String facultet;
  final String course;
  final String groupe;
  final String specialization;

  ProfileModel(
      {this.surname = "Фамилия",
      this.name = "Имя",
      this.lastName = "Отчество",
      this.status = "Учится",
      this.gender = "Женский",
      this.code = "ФИТ-19-0032",
      this.facultet = "Факультет информационных технологий",
      this.course = "4",
      this.groupe = "191-331",
      this.specialization =
          "10.05.03 Информационная безопасность автоматизированных систем",
      this.dateBirth = "17.02.2001"});
}
