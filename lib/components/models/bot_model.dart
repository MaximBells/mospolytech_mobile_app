class BotModel {
  final String title;
  final String image;

  BotModel({required this.title, required this.image});
}
