import 'package:flutter/cupertino.dart';

class TextModel {
  final String name;
  String? errorText;
  bool validated = false;
  String text = "";
  final TextEditingController controller = TextEditingController();

  TextModel({required this.name, this.errorText, this.text = ""});
}
