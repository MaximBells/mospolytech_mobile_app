import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/components/models/bot_model.dart';
import 'package:polytech_app/static/app_image.dart';

class BotValue {
  List<BotModel> data = [
    BotModel(title: "Профиль", image: AppImage.profile),
    BotModel(title: "Главная", image: AppImage.home),
    BotModel(title: "Расписание", image: AppImage.shedule),
    BotModel(title: "Все разделы", image: AppImage.allGroups),
  ];
  int activeIndex = 0;
  BotModel activeModel = BotModel(title: "Главная", image: AppImage.home);
}

class BotController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    activeModel = data.first;
  }

  final _botValue = BotValue().obs;

  List<BotModel> get data => _botValue.value.data;

  void onTap(int index) {
    activeIndex = index;
    activeModel = data[activeIndex];
    Get.find<MainController>().activeScreen =
        Get.find<MainController>().screens[activeIndex];
    update();
  }

  set data(List<BotModel> value) {
    _botValue.update((val) {
      val?.data = value;
    });
    update();
  }

  int get activeIndex => _botValue.value.activeIndex;

  set activeIndex(int value) {
    _botValue.update((val) {
      val?.activeIndex = value;
    });
    update();
  }

  List<TabItem> get items => data
      .map((e) => TabItem(
          icon: Image.asset(e.image),
          title: e.title,
          fontFamily: GoogleFonts.montserrat().fontFamily))
      .toList();

  BotModel get activeModel => _botValue.value.activeModel;

  set activeModel(BotModel value) {
    _botValue.update((val) {
      val?.activeModel = value;
    });
    update();
  }
}
