import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_controller.dart';
import 'package:polytech_app/static/app_color.dart';

class BotBar extends StatelessWidget {
  const BotBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final BotController controller = Get.find<BotController>();
    return ConvexAppBar(
      backgroundColor: AppColor.black,
      activeColor: AppColor.black,
      color: AppColor.white,
      items: controller.items,
      curveSize: 0,
      style: TabStyle.custom,
      onTap: (int i) {
        controller.onTap(i);
      },
    );
  }
}
