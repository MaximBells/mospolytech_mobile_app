import 'package:flutter/material.dart';

class Margin extends StatelessWidget {
  const Margin({Key? key, required this.child, this.margin}) : super(key: key);
  final Widget child;
  final EdgeInsetsGeometry? margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: child,
    );
  }
}
