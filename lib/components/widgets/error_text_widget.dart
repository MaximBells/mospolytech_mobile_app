import 'package:flutter/material.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class ErrorTextWidget extends StatelessWidget {
  const ErrorTextWidget({Key? key, this.errorText, this.validated = false})
      : super(key: key);
  final String? errorText;
  final bool validated;

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 200),
      child: validated && errorText != null
          ? Container(
              alignment: Alignment.bottomLeft,
              child: const TextWidget(
                text: "Поле должно быть заполнено",
                textColor: AppColor.red,
                maxLines: null,
                fontWeight: FontWeight.w600,
                margin: EdgeInsets.only(right: 32, left: 8),
              ))
          : const SizedBox(),
    );
  }
}
