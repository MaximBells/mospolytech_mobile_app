import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/components/widgets/button.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';

class BottomButton extends StatelessWidget {
  const BottomButton(
      {Key? key, this.buttonColor, this.textButton, required this.onPressed})
      : super(key: key);
  final Color? buttonColor;
  final String? textButton;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 100),
      child: MediaQuery.of(context).viewInsets.bottom == 0
          ? Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              height: Get.height * 0.15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Button(
                    onPressed: () {
                      onPressed();
                    },
                    backgroundColor: buttonColor,
                    child: TextWidget(
                      text: textButton ?? "Вход",
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            )
          : const SizedBox(),
    );
  }
}
