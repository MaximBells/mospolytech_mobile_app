import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_controller.dart';
import 'package:polytech_app/static/app_color.dart';

class NameWidget extends StatelessWidget {
  const NameWidget({Key? key, this.margin, this.size = 40, this.fontSize = 16})
      : super(key: key);
  final EdgeInsetsGeometry? margin;
  final double size;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: size,
        height: size,
        margin: margin,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color:
                AppColor.values[Random().nextInt(AppColor.values.length - 1)]),
        child: Center(
          child: Text(
            "${Get.find<MainController>().profile.surname[0]}${Get.find<MainController>().profile.name[0]}",
            style: GoogleFonts.montserrat(
              color: AppColor.white,
              fontWeight: FontWeight.bold,
              fontSize: fontSize,
            ),
          ),
        ));
  }
}
