import 'package:flutter/services.dart';
import 'package:get/get.dart';

class TextValue {
  String? errorText = "";
  String text = "";
}

class TextController extends GetxController {
  final _textValue = TextValue().obs;
  List<TextInputFormatter> formatters = [];

  final RegExp? regExp;

  TextController({this.regExp});

  String? get errorText => _textValue.value.errorText;

  @override
  void onInit() {
    if (regExp != null) {
      formatters.add(FilteringTextInputFormatter.allow(regExp!));
    }
    super.onInit();
  }

  set errorText(String? value) {
    _textValue.update((val) {
      val?.errorText = value;
    });
    update();
  }

  String get text => _textValue.value.text;

  set text(String value) {
    _textValue.update((val) {
      val?.text = value;
    });
    update();
  }

  void updateText(String value) {
    text = value;
    if (text.isEmpty) {
      errorText = "Ошибка! Поле должно быть заполнено...";
    } else {
      errorText = null;
    }
  }
}
