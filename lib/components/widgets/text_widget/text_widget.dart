import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/static/app_color.dart';

class TextWidget extends StatelessWidget {
  const TextWidget(
      {Key? key,
      required this.text,
      this.fontWeight,
      this.fontStyle,
      this.textColor,
      this.margin,
      this.maxLines,
      this.textAlign,
      this.fontSize = 16})
      : super(key: key);
  final String text;
  final FontWeight? fontWeight;
  final double fontSize;
  final FontStyle? fontStyle;
  final Color? textColor;
  final EdgeInsetsGeometry? margin;
  final int? maxLines;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) {
    return Margin(
      margin: margin,
      child: AutoSizeText(
        text,
        maxLines: maxLines,
        minFontSize: fontSize,
        textAlign: textAlign,
        style: GoogleFonts.montserrat(
            color: textColor ?? AppColor.white,
            fontStyle: fontStyle,
            fontWeight: fontWeight,
            fontSize: fontSize),
      ),
    );
  }
}
