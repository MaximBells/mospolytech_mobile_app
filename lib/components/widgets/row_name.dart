import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_controller.dart';
import 'package:polytech_app/components/widgets/name_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class RowName extends StatelessWidget {
  const RowName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => AnimatedOpacity(
          duration: const Duration(milliseconds: 150),
          opacity: Get.find<BotController>().activeIndex == 0 ? 0 : 1,
          child: Row(
            children: [
              const NameWidget(
                margin: EdgeInsets.only(
                  right: 16,
                ),
                fontSize: 16,
              ),
              Text(
                Get.find<MainController>().profile.name,
                style: GoogleFonts.montserrat(
                    color: AppColor.white, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ));
  }
}
/*
AnimatedCrossFade(
        firstChild: const SizedBox(),
        secondChild: Row(
          children: [
            const NameWidget(
              margin: EdgeInsets.only(right: 16),
            ),
            Text(
              Get.find<MainController>().profile.name,
              style: GoogleFonts.montserrat(
                  color: AppColor.white, fontWeight: FontWeight.bold),
            )
          ],
        ),
        crossFadeState: Get.find<BotController>().activeIndex == 0
            ? CrossFadeState.showFirst
            : CrossFadeState.showSecond,
        duration: const Duration(milliseconds: 200))
 */
