import 'package:flutter/material.dart';
import 'package:polytech_app/static/app_color.dart';

class RectangleIcon extends StatelessWidget {
  const RectangleIcon(
      {Key? key, this.width = 32, this.height = 32, this.icon = Icons.close})
      : super(key: key);
  final double height;
  final double width;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6), color: AppColor.black),
      child: Center(
        child: Icon(
          icon,
          color: AppColor.white,
          size: 22,
        ),
      ),
    );
  }
}
