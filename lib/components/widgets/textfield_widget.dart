import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/components/widgets/error_text_widget.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_controller.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class TextFieldWidget extends StatelessWidget {
  const TextFieldWidget(
      {Key? key,
      this.helperText,
      this.textEditingController,
      this.onSuffixTap,
      this.onTap,
      this.hidden,
      this.suffixIcon,
      this.onChanged,
      this.onError,
      this.validated = false,
      this.regExp,
      this.margin})
      : super(key: key);
  final String? helperText;
  final TextEditingController? textEditingController;
  final VoidCallback? onTap;
  final Widget? suffixIcon;
  final VoidCallback? onSuffixTap;
  final bool? hidden;
  final EdgeInsetsGeometry? margin;
  final Function(String)? onChanged;
  final Function(String?)? onError;
  final bool validated;
  final RegExp? regExp;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TextController>(
        init: TextController(
          regExp: regExp,
        ),
        global: false,
        builder: (controller) {
          return Margin(
            margin: margin,
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.centerRight,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(6),
                      decoration: BoxDecoration(
                          color: AppColor.lightBlack,
                          borderRadius: BorderRadius.circular(14)),
                      child: TextField(
                        onTap: () {
                          if (onTap != null) {
                            onTap!();
                          }
                        },
                        onChanged: (value) {
                          controller.updateText(value);
                          if (onChanged != null) {
                            onChanged!(value);
                          }
                          if (onError != null) {
                            onError!(controller.errorText);
                          }
                        },
                        obscureText: hidden ?? false,
                        controller: textEditingController,
                        style: GoogleFonts.montserrat(
                            color: AppColor.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18),
                        cursorColor: AppColor.darkWhite,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.only(
                                left: 12, top: 14, bottom: 14),
                            helperText: helperText,
                            helperMaxLines: 2,
                            filled: true,
                            fillColor: AppColor.darkWhite,
                            helperStyle: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                color: Colors.grey),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(14),
                                borderSide: BorderSide.none),
                            labelStyle: const TextStyle(fontSize: 14),
                            hintStyle: const TextStyle(fontSize: 24)),
                      ),
                    ),
                    Margin(
                        margin: const EdgeInsets.only(
                          right: 12,
                        ),
                        child: GestureDetector(
                            onTap: () {
                              if (onSuffixTap != null) {
                                onSuffixTap!();
                              }
                            },
                            child: suffixIcon ?? Container()))
                  ],
                ),
                ErrorTextWidget(
                  errorText: controller.errorText,
                  validated: validated,
                )
              ],
            ),
          );
        });
  }
}
