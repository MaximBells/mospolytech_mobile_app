import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/auth/screens/auth_screen.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class Utils {
  static Color get randomColor =>
      Colors.primaries[Random().nextInt(Colors.primaries.length)];

  static void showDialog() {
    Get.dialog(
        Dialog(
          backgroundColor: AppColor.black,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
            height: Get.height * 0.175,
            width: Get.width * 0.175,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const TextWidget(
                  text: "Вы точно хотите выйти из аккаунта?",
                  textColor: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.pop(Get.context!);
                          Navigator.pushReplacement(
                              Get.context!,
                              CupertinoPageRoute(
                                  builder: (context) => const AuthScreen()));
                        },
                        child: const TextWidget(
                          text: "Да",
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          textColor: AppColor.green,
                        )),
                    const SizedBox(
                      width: 10,
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.pop(Get.context!);
                        },
                        child: const TextWidget(
                          text: "Нет",
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          textColor: AppColor.red,
                        ))
                  ],
                )
              ],
            ),
          ),
        ),
        barrierDismissible: false);
  }
}
