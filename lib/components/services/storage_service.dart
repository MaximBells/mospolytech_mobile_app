import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:polytech_app/components/enums/storage_enum.dart';
import 'package:polytech_app/components/services/crypt_service.dart';

class StorageService {
  final _storage = const FlutterSecureStorage();

  Future<String?> getValue(StorageEnum key) async {
    return await _storage.read(key: key.hash);
  }

  Future<void> setValue(StorageEnum key, dynamic value,
      {bool needToEncrypt = true}) async {
    if (needToEncrypt == true) {
      _storage.write(
          key: key.hash,
          value: Get.find<CryptService>()
              .encrypt(value.toString(), Get.find<CryptService>().password));
    } else {
      _storage.write(key: key.hash, value: value.toString());
    }
  }

  Future<void> remove(StorageEnum key) async {
    await _storage.delete(key: key.hash);
  }

  Future<void> removeAll() async {
    await _storage.deleteAll();
  }

  Future<bool> get hasLogin async => await getValue(StorageEnum.login) != null;
}
