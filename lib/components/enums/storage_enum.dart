import 'package:polytech_app/components/services/crypt_service.dart';
import 'package:polytech_app/static/di.dart';

enum StorageEnum {
  login,
  password,
  auth;

  String get hash => getIt<CryptService>().generateHash(name);


  const StorageEnum();
}
