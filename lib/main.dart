import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/loading/screens/loading_screen.dart';
import 'package:polytech_app/static/app_color.dart';
import 'package:polytech_app/static/di.dart';

Future<void> main() async {
  await initServices();
  runApp(GetMaterialApp(
    theme: ThemeData(
        scaffoldBackgroundColor: AppColor.black,
        colorScheme: ColorScheme.fromSwatch(
            brightness: Brightness.dark,
            primarySwatch: AppColor.blackScheme,
            backgroundColor: AppColor.black)),
    home: LoadingScreen(),
  ));
}
