import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/home/widgets/rent_widget.dart';
import 'package:polytech_app/blocks/home/widgets/shedule_widget.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/blocks/profile/widgets/qr_widget.dart';
import 'package:polytech_app/components/widgets/name_widget.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
          width: double.infinity,
          child: Column(
            children: [
              const QrWidget(
                margin: EdgeInsets.only(top: 20),
              ),
              SheduleWidget(
                  time: "12:20 - 13:50",
                  name: "Название дисциплины",
                  teacher: "ФИО Преподавателя",
                  place: "ав1201",
                  date: "10 Апр - 05 Июн",
                  timeBefore: "через 9ч. 14мин."),
              RentWidget()
            ],
          )),
    );
  }
}
