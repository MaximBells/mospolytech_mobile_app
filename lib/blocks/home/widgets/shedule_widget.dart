import 'package:flutter/material.dart';
import 'package:polytech_app/components/services/utils.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class SheduleWidget extends StatelessWidget {
  const SheduleWidget(
      {Key? key,
      required this.time,
      required this.name,
      required this.teacher,
      required this.place,
      required this.date,
      required this.timeBefore})
      : super(key: key);
  final String time;
  final String timeBefore;
  final String place;
  final String name;
  final String teacher;
  final String date;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(4.5),
      margin: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: AppColor.black,
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: AppColor.lightBlack,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                  decoration: BoxDecoration(
                    color: Utils.randomColor,
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: TextWidget(
                    text: time,
                    textColor: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(right: 6),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                  decoration: BoxDecoration(
                    color: AppColor.black,
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: TextWidget(
                    text: place,
                    textColor: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                )
              ],
            ),
            TextWidget(
              margin: const EdgeInsets.symmetric(vertical: 12),
              text: name,
              textColor: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            TextWidget(
              margin: const EdgeInsets.only(bottom: 4),
              text: teacher,
              textColor: Colors.white,
              fontSize: 18,
              fontWeight: FontWeight.normal,
            ),
            TextWidget(
              margin: const EdgeInsets.only(bottom: 4),
              text: date,
              textColor: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 6),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                  decoration: BoxDecoration(
                    color: Utils.randomColor,
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: TextWidget(
                    text: timeBefore,
                    textColor: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
