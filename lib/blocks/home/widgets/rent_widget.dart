import 'package:flutter/material.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class RentWidget extends StatelessWidget {
  const RentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(4.5),
      margin: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: AppColor.black,
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: AppColor.lightBlack,
        ),
        child: Column(
          children: [
            Margin(
              margin: const EdgeInsets.only(bottom: 42),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWidget(
                        text: "Общежитие",
                        textColor: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                      ),
                      TextWidget(
                        text: "0 руб.",
                        textColor: AppColor.green,
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ],
                  ),
                  IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.open_in_new_rounded,
                        color: Colors.white,
                        size: 30,
                      ))
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 6),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColor.green.withOpacity(0.15),
                  borderRadius: BorderRadius.circular(12)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.done,
                    color: AppColor.green,
                    size: 16,
                  ),
                  TextWidget(
                    text: "Оплачено",
                    textColor: AppColor.green,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    margin: const EdgeInsets.only(left: 6),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
