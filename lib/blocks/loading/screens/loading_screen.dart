import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/loading/data/loading_controller.dart';
import 'package:polytech_app/blocks/loading/widgets/logo_widget.dart';
import 'package:polytech_app/components/widgets/mospolytech.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LoadingController loadingController = Get.put(LoadingController());
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const Expanded(flex: 1, child: SizedBox()),
            Expanded(
              flex: 3,
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 72, vertical: 36),
                child: const LoadingSpinWidget(),
              ),
            ),
            const Expanded(
                flex: 1,
                child: TextWidget(
                  text: "Made by Mospolytech",
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ))
          ],
        ),
      ),
    );
  }
}
