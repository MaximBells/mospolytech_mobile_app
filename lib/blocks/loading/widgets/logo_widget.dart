import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:polytech_app/components/widgets/mospolytech.dart';

class LoadingSpinWidget extends StatelessWidget {
  const LoadingSpinWidget({Key? key, this.height = 300, this.width = 300})
      : super(key: key);
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Spin(
      delay: const Duration(milliseconds: 500),
      duration: const Duration(milliseconds: 2000),
      infinite: true,
      child: Mospolytech(height: height, width: width),
    );
  }
}
