import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/auth/screens/auth_screen.dart';
import 'package:polytech_app/blocks/main/screens/main_screen.dart';
import 'package:polytech_app/components/services/storage_service.dart';

class LoadingValue {}

class LoadingController extends GetxController {
  final _loadingValue = LoadingValue().obs;

  void initNextScreen() {
    Future.delayed(const Duration(seconds: 3)).then((value) {
      if (Get.find<StorageService>().hasLogin == true) {
        Navigator.pushReplacement(Get.context!,
            CupertinoPageRoute(builder: (context) => const MainScreen()));
      } else {
        Navigator.pushReplacement(Get.context!,
            CupertinoPageRoute(builder: (context) => const AuthScreen()));
      }
    });
  }

  @override
  void onInit() {
    super.onInit();
    initNextScreen();
  }
}
