import 'package:flutter/material.dart';
import 'package:polytech_app/components/services/utils.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class CardShedule extends StatelessWidget {
  const CardShedule(
      {Key? key,
      required this.name,
      required this.date,
      required this.place,
      required this.teacher,
      required this.time})
      : super(key: key);
  final String time;
  final String name;
  final String teacher;
  final String date;
  final String place;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
      padding: const EdgeInsets.only(left: 12, right: 12, top: 12, bottom: 10),
      decoration: BoxDecoration(
          color: AppColor.lightBlack, borderRadius: BorderRadius.circular(12)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          TextWidget(
            text: name,
            textColor: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold,
            maxLines: null,
            margin: const EdgeInsets.only(bottom: 12),
          ),
          TextWidget(
            text: teacher,
            textColor: AppColor.gray,
            fontSize: 18,
            fontWeight: FontWeight.bold,
            maxLines: null,
            margin: const EdgeInsets.only(bottom: 2),
          ),
          TextWidget(
            text: date,
            textColor: AppColor.gray,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            maxLines: null,
            margin: const EdgeInsets.only(bottom: 32),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                decoration: BoxDecoration(
                    color: Utils.randomColor,
                    borderRadius: BorderRadius.circular(32)),
                child: TextWidget(
                  text: time,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  textColor: Colors.white,
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                decoration: BoxDecoration(
                    color: AppColor.lightBlue,
                    borderRadius: BorderRadius.circular(12)),
                child: TextWidget(
                  text: place,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                  textColor: Colors.white,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
