import 'package:flutter/material.dart';
import 'package:polytech_app/components/services/utils.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class DayShedule extends StatelessWidget {
  const DayShedule(
      {Key? key, required this.name, required this.count, required this.time})
      : super(key: key);
  final String name;
  final String count;
  final String time;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      decoration: BoxDecoration(
          color: AppColor.lightBlack, borderRadius: BorderRadius.circular(12)),
      child: Column(
        children: [
          Margin(
            margin: const EdgeInsets.only(bottom: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextWidget(
                  text: name,
                  textColor: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
                TextWidget(
                  text: count,
                  textColor: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                decoration: BoxDecoration(
                    color: Utils.randomColor,
                    borderRadius: BorderRadius.circular(32)),
                child: TextWidget(
                  text: time,
                  textColor: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
