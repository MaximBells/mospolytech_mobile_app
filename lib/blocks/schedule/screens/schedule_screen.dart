import 'package:flutter/material.dart';
import 'package:polytech_app/blocks/schedule/widgets/card_shedule.dart';
import 'package:polytech_app/blocks/schedule/widgets/day_shedule.dart';

class ScheduleScreen extends StatelessWidget {
  const ScheduleScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          DayShedule(name: "Вторник", count: "n пар", time: "09:00 - 10:30"),
          CardShedule(
              name: "Полное название дисциплины\n(Лекция)",
              date: "06 Фев - 05 Июн",
              place: "ав1204",
              teacher: "ФИО преподавателя",
              time: "09:00 - 10:30"),
          CardShedule(
              name: "Полное название дисциплины\n(Лекция)",
              date: "06 Фев - 05 Июн",
              place: "ав1204",
              teacher: "ФИО преподавателя",
              time: "10:40 - 12:10"),
          CardShedule(
              name: "Полное название дисциплины\n(Лекция)",
              date: "06 Фев - 05 Июн",
              place: "ав1204",
              teacher: "ФИО преподавателя",
              time: "12:20 - 13:50")
        ],
      ),
    );
  }
}
