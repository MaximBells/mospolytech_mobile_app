import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/blocks/auth/screens/auth_screen.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/components/services/utils.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_bar.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_controller.dart';
import 'package:polytech_app/components/widgets/name_widget.dart';
import 'package:polytech_app/components/widgets/row_name.dart';
import 'package:polytech_app/static/app_color.dart';
import 'package:polytech_app/static/app_image.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(MainController());
    return Scaffold(
      bottomNavigationBar: const BotBar(),
      appBar: AppBar(
        title: const RowName(),
        actions: [
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.settings,
                color: AppColor.white,
              )),
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.notifications_outlined,
                color: AppColor.white,
              )),
          IconButton(
              onPressed: () {
                Utils.showDialog();

              },
              icon: const Icon(
                Icons.exit_to_app,
                color: AppColor.white,
              ))
        ],
      ),
      body: Center(
        child: Obx(() => Get.find<MainController>().activeScreen),
      ),
    );
  }
}
