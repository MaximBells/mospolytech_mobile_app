import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/groups/screens/groups_screens.dart';
import 'package:polytech_app/blocks/home/screens/home_screen.dart';
import 'package:polytech_app/blocks/profile/screens/profile_screen.dart';
import 'package:polytech_app/blocks/schedule/screens/schedule_screen.dart';
import 'package:polytech_app/components/models/profile_model.dart';
import 'package:polytech_app/components/widgets/bot_bar/bot_controller.dart';

class MainValue {
  ProfileModel profileModel = ProfileModel();
  Widget activeScreen = Container();
}

class MainController extends GetxController {
  String get fullName =>
      "${profile.surname} ${profile.name} ${profile.lastName}";

  List<Widget> screens = [
    const ProfileScreen(),
    const HomeScreen(),
    const ScheduleScreen(),
    const GroupsScreen(),
  ];

  final _mainValue = MainValue().obs;

  ProfileModel get profile => _mainValue.value.profileModel;

  set profile(ProfileModel value) {
    _mainValue.update((val) {
      val?.profileModel = value;
    });
    update();
  }

  Widget get activeScreen => _mainValue.value.activeScreen;

  set activeScreen(Widget value) {
    _mainValue.update((val) {
      val?.activeScreen = value;
    });
    update();
  }

  @override
  void onInit() {
    super.onInit();
    Get.put(BotController());
    activeScreen = screens.first;
  }
}
