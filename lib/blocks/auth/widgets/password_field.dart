import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/auth/data/auth_controller.dart';
import 'package:polytech_app/components/widgets/rectangle_icon.dart';
import 'package:polytech_app/components/widgets/textfield_widget.dart';

class PasswordField extends StatefulWidget {
  const PasswordField({Key? key}) : super(key: key);

  @override
  State<PasswordField> createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  bool _hidden = true;

  @override
  Widget build(BuildContext context) {
    final AuthController authController = Get.find<AuthController>();
    return Obx(() => TextFieldWidget(
        regExp:
            RegExp(r'^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\d)(?=.*[!#$%&? "]).*$'),
        hidden: _hidden,
        margin: const EdgeInsets.only(top: 20),
        validated: authController.passwordValidated,
        onChanged: (value) {
          authController.passwordText = value;
        },
        onTap: () {
          authController.passwordValidated = true;
        },
        onError: (value) {
          authController.updatePasswordErrorText(value);
        },
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              _hidden = !_hidden;
            });
          },
          child: AnimatedSwitcher(
            duration: const Duration(milliseconds: 200),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return ScaleTransition(scale: animation, child: child);
            },
            child: _hidden
                ? const RectangleIcon(
                    icon: Icons.visibility,
                  )
                : Container(
                    child: const RectangleIcon(
                      icon: Icons.visibility_off,
                    ),
                  ),
          ),
        )));
  }
}
