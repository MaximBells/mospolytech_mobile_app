import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/auth/data/auth_controller.dart';
import 'package:polytech_app/components/widgets/rectangle_icon.dart';
import 'package:polytech_app/components/widgets/textfield_widget.dart';

class LoginField extends StatefulWidget {
  const LoginField({Key? key}) : super(key: key);

  @override
  State<LoginField> createState() => _LoginFieldState();
}

class _LoginFieldState extends State<LoginField> {
  String _text = "";

  @override
  Widget build(BuildContext context) {
    final AuthController authController = Get.find<AuthController>();
    return Obx(() {
      return TextFieldWidget(
        regExp: RegExp(r'/[a-zA-Z]+/g'),
        validated: authController.loginValidated,
        onSuffixTap: () {
          authController.loginController.clear();
          setState(() {
            _text = authController.loginController.text;
          });
          authController.loginText = _text;
          authController.updateLoginErrorText(_text);
        },
        onTap: () {
          authController.loginValidated = true;
        },
        onChanged: (value) {
          setState(() {
            _text = value;
          });
          authController.loginText = _text;
        },
        textEditingController: authController.loginController,
        onError: (value) {
          authController.updateLoginErrorText(value);
        },
        margin: const EdgeInsets.only(top: 20),
        suffixIcon: AnimatedSwitcher(
          duration: const Duration(milliseconds: 200),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(scale: animation, child: child);
          },
          child: _text.isNotEmpty
              ? const RectangleIcon(
                  icon: Icons.close,
                )
              : null,
        ),
      );
    });
  }
}
