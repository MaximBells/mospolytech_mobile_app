import 'package:flutter/material.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class CheckboxRow extends StatefulWidget {
  const CheckboxRow(
      {Key? key,
      required this.text,
      this.textColor,
      this.fontSize = 16,
      this.margin,
      this.fontWeight})
      : super(key: key);
  final String text;
  final Color? textColor;
  final FontWeight? fontWeight;
  final double fontSize;
  final EdgeInsetsGeometry? margin;

  @override
  State<CheckboxRow> createState() => _CheckboxRowState();
}

class _CheckboxRowState extends State<CheckboxRow> {
  bool _checkBoxValue = true;

  @override
  Widget build(BuildContext context) {
    return Margin(
      margin: widget.margin,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Checkbox(
            checkColor: AppColor.black,
            activeColor: AppColor.blue,
            shape: CircleBorder(),
              value: _checkBoxValue,
              onChanged: (bool? value) {
                setState(() {
                  _checkBoxValue = value ?? false;
                });
              }),
          TextWidget(
            text: widget.text,
            textColor: widget.textColor,
            fontWeight: widget.fontWeight,
            fontSize: widget.fontSize,
          )
        ],
      ),
    );
  }
}
