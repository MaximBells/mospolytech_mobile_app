import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:polytech_app/components/models/text_model.dart';

class AuthValue {}

class AuthController extends GetxController {
  final _authValue = AuthValue().obs;

  final login = TextModel(name: "Login").obs;
  final password = TextModel(name: "Password").obs;

  void updateLoginErrorText(String? value) {
    login.update((val) {
      val?.errorText = value;
    });
  }

  void updatePasswordErrorText(String? value) {
    password.update((val) {
      val?.errorText = value;
    });
  }

  bool get loginValidated => login.value.validated;

  bool get passwordValidated => password.value.validated;

  set loginValidated(bool value) {
    login.update((val) {
      val?.validated = value;
    });
    update();
  }

  set passwordValidated(bool value) {
    password.update((val) {
      val?.validated = value;
    });
    update();
  }

  TextEditingController get loginController => login.value.controller;

  TextEditingController get passwordController => password.value.controller;

  bool get loginHasError => login.value.errorText != null;

  bool get passwordHasError => password.value.errorText != null;

  String get loginText => login.value.text;

  set loginText(String value) {
    login.update((val) {
      val?.text = value;
    });
    update();
  }

  String get passwordText => password.value.text;

  set passwordText(String value) {
    password.update((val) {
      val?.text = value;
    });
    update();
  }
}
