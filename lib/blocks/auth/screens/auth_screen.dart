import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/auth/data/auth_controller.dart';
import 'package:polytech_app/blocks/auth/widgets/checkbox_row.dart';
import 'package:polytech_app/blocks/auth/widgets/login_field.dart';
import 'package:polytech_app/blocks/auth/widgets/password_field.dart';
import 'package:polytech_app/blocks/main/screens/main_screen.dart';
import 'package:polytech_app/components/widgets/bottom_button.dart';
import 'package:polytech_app/components/widgets/rectangle_icon.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/components/widgets/textfield_widget.dart';
import 'package:polytech_app/static/app_color.dart';
import 'package:polytech_app/static/app_image.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AuthController authController = Get.put(AuthController());
    return Scaffold(
      bottomSheet: BottomButton(
        textButton: "Вход",
        buttonColor: AppColor.lightBlue,
        onPressed: () {
          Get.find<AuthController>().loginValidated = true;
          Get.find<AuthController>().passwordValidated = true;
          if (authController.loginText.isNotEmpty &&
              authController.passwordText.isNotEmpty) {
            Navigator.pushReplacement(context,
                CupertinoPageRoute(builder: (context) => const MainScreen()));
            Get.find<AuthController>().loginValidated = false;
            Get.find<AuthController>().passwordValidated = false;
          }
        },
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 20),
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.asset(
                          AppImage.logoMin,
                          width: 64,
                          height: 64,
                        ),
                        const TextWidget(
                          text: "Личный кабинет",
                          fontWeight: FontWeight.w800,
                          fontSize: 20,
                          margin: EdgeInsets.symmetric(vertical: 10),
                        ),
                      ],
                    ),
                  ),
                  const TextWidget(
                    text: "Вход",
                    fontWeight: FontWeight.w800,
                    fontSize: 18,
                  ),
                  const TextWidget(
                    text:
                        "Вход в личный кабинет происходит через единую учетную запись (ЕУЗ)",
                    fontWeight: FontWeight.w600,
                    textColor: AppColor.gray,
                    margin: EdgeInsets.only(top: 14),
                  ),
                  const TextWidget(
                    text: "Логин",
                    fontWeight: FontWeight.w800,
                    fontSize: 18,
                    margin: EdgeInsets.only(top: 20),
                  ),
                  const LoginField(),
                  const TextWidget(
                    text: "Пароль",
                    fontWeight: FontWeight.w800,
                    fontSize: 18,
                    margin: EdgeInsets.only(top: 20),
                  ),
                  const PasswordField(),
                  const CheckboxRow(
                    text: "Оставаться в системе",
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
