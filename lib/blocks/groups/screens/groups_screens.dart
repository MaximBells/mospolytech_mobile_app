import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:polytech_app/blocks/groups/widgets/double_group.dart';
import 'package:polytech_app/blocks/groups/widgets/solid_group.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_image.dart';

class GroupsScreen extends StatelessWidget {
  const GroupsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
          child: ListView(
            children: [
              Margin(
                margin: const EdgeInsets.only(bottom: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const TextWidget(
                      text: "Все разделы",
                      textColor: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      margin: const EdgeInsets.symmetric(vertical: 12),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        SolidGroup(name: "Настройки", image: AppImage.settings),
                        SolidGroup(name: "Главная", image: AppImage.home),
                        SolidGroup(name: "Профиль", image: AppImage.profile),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        SolidGroup(name: "Расписание", image: AppImage.shedule),
                        DoubleGroup(
                            image: AppImage.medicine,
                            name: "Получение\nмедицинских справок"),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        SolidGroup(
                            name: "Полезная\nинформация", image: AppImage.faq),
                        SolidGroup(name: "Главная", image: AppImage.success),
                        SolidGroup(name: "Профиль", image: AppImage.pd),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const [
                        SolidGroup(name: "Договоры", image: AppImage.ruble),
                        SolidGroup(name: "Сотрудники", image: AppImage.workers),
                        SolidGroup(name: "Сообщения", image: AppImage.chat),
                      ],
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
