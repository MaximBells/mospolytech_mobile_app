import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';

class SolidGroup extends StatelessWidget {
  const SolidGroup({Key? key, required this.name, required this.image})
      : super(key: key);
  final String image;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: Get.width * 0.29,
      height: Get.height * 0.175,
      margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 2),
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 12),
      decoration: BoxDecoration(
          color: AppColor.lightBlack, borderRadius: BorderRadius.circular(12)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            image,
            width: Get.width * 0.125,
          ),
          const SizedBox(
            height: 6,
          ),
          TextWidget(
            text: name,
            textColor: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 14,
            maxLines: null,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
