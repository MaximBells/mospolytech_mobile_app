import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:polytech_app/blocks/main/data/main_controller.dart';
import 'package:polytech_app/blocks/profile/widgets/property_widget.dart';
import 'package:polytech_app/blocks/profile/widgets/qr_widget.dart';
import 'package:polytech_app/components/widgets/name_widget.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
          width: double.infinity,
          child: Column(
            children: [
              const NameWidget(
                size: 80,
                fontSize: 30,
                margin: EdgeInsets.only(top: 10),
              ),
              TextWidget(
                margin: const EdgeInsets.only(top: 16),
                text: Get.find<MainController>().fullName,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
              const QrWidget(
                margin: EdgeInsets.only(top: 20),
              ),
              PropertyWidget(
                profile: Get.find<MainController>().profile,
                margin:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
              )
            ],
          )),
    );
  }
}
