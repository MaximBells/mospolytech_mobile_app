import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:polytech_app/components/models/profile_model.dart';
import 'package:polytech_app/components/widgets/margin.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';

class PropertyWidget extends StatelessWidget {
  const PropertyWidget({Key? key, required this.profile, this.margin})
      : super(key: key);
  final ProfileModel profile;
  final EdgeInsetsGeometry? margin;

  TextWidget keyText(String text) => TextWidget(
        text: "$text: ",
        fontWeight: FontWeight.bold,
        fontSize: 16,
      );

  TextWidget valueText(String text) => TextWidget(
        text: text,
        fontSize: 16,
        maxLines: null,
      );

  Widget rowText(String key, String value) => RichText(
          text: TextSpan(children: [
        TextSpan(text: "$key: ", style: TextStyle(fontWeight: FontWeight.bold)),
        TextSpan(text: value)
      ], style: GoogleFonts.montserrat(fontSize: 16)));

  @override
  Widget build(BuildContext context) {
    return Margin(
        margin: margin,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            rowText("Статус", profile.status),
            rowText("Пол", profile.gender),
            rowText("Дата рождения", profile.dateBirth),
            rowText("Код студента", profile.code),
            rowText("Факультет", profile.facultet),
            rowText("Курс", profile.course),
            rowText("Группа", profile.groupe),
            rowText("Направление", profile.specialization)
          ],
        ));
  }
}
