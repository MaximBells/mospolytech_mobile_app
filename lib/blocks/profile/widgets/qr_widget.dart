import 'package:flutter/material.dart';
import 'package:polytech_app/components/widgets/text_widget/text_widget.dart';
import 'package:polytech_app/static/app_color.dart';
import 'package:polytech_app/static/app_image.dart';

class QrWidget extends StatelessWidget {
  const QrWidget({Key? key, this.margin}) : super(key: key);
  final EdgeInsetsGeometry? margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: const EdgeInsets.only(right: 24, left: 24, top: 8, bottom: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: AppColor.lightBlack,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const TextWidget(
            text: "QR-код",
            fontSize: 20,
            fontWeight: FontWeight.bold,
            margin: EdgeInsets.only(bottom: 16),
          ),
          Image.asset(
            AppImage.qrCode,
            width: 120,
            height: 120,
          )
        ],
      ),
    );
  }
}
